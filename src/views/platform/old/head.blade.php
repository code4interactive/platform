<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
        <meta http-equiv="content-language" content="pl" />
        <link href="/images/c4Ico32x32.png" rel="shortcut icon" />
        
        <title>Title</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap/css/bootstrap.css') }}">

        <script src="{{ asset('scripts/jquery-1.10.0.min.js') }}"></script>
        <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>

    </head>
<body>