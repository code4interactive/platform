<?php namespace Code4\Platform;

class Models_Users extends \Eloquent  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
}