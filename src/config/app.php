<?php

return array(

	'publicViewsPath' => '/packages/code4/platform/views/',
	'packagePath' => '/packages/code4/platform',
	'themesPath' => '/packages/code4/platform/assets/'

);