### Install & Configure

You can use Cartalyst's Data Grid package both with Laravel 4 and outside by installing the package through Composer.

- [Install through Composer](/data-grid/installation/composer)
- [Install in Laravel 4](/data-grid/installation/laravel-4)