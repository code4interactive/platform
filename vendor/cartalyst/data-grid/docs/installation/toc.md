## Install & Configure

- [Installation](/data-grid/installation)
- [Composer](/data-grid/installation/composer)
- [Laravel 4](/data-grid/installation/laravel-4)
