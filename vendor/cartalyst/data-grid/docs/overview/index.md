<a name="introduction"></a>
### Introduction

Cartalyst's Data Grid packages makes it easy for you to filter data sources. Data Grid shifts the focus from pagination to data filtration. Pass any data source through a data handler and the package will take care of the rest so you can use the filtered result set to create your applications.

We emphasize filtering first, sorting second, and lastly, paginating those results.

<a name="features"></a>
### Features

- Easily filter large data sources
- Create JSON responses to use in your API
- Build paginated result sets

<a name="requirements"></a>
### Requirements

- PHP >= 5.3.0

To use Cartalyst's Data Grid package you need to have a valid Cartalyst.com subscription. Click [here](https://www.cartalyst.com/pricing) to obtain your subscription.

<a name="license"></a>
### License

Cartalyst's Data Grid package is licensed under [the BSD 3-Clause license](/data-grid/overview/license).
